import java.util.Scanner;
public class TicTacToeGame {
	
	public static void main(String[] args) {
		System.out.println("Welcome to Tic-tac-toe !");
		System.out.println("Player 1's token: X");
		System.out.println("Player 2's token: O");
		Board b = new Board();
		boolean gameOver = false;
		int player = 1;
		Square playerToken = Square.X;
		Scanner r = new Scanner(System.in);
		
		while (gameOver == false) {
			System.out.println(b);
			if (player == 1) {
				playerToken = Square.X;
			} else {
				playerToken = Square.O;
			}
			System.out.println("Player "+player+": it's your turn where do you want to place your token?");
			int row = r.nextInt();
			System.out.println("Insert the column of your token: ");
			int col = r.nextInt();
			while (b.placeToken(row, col, playerToken) == false) {
				System.out.println("This space is already taken");
				System.out.println("Reenter your row: ");
				int newRow = r.nextInt();
				System.out.println("Reenter your column: ");
				int newCol = r.nextInt();	
				b.placeToken(newRow, newCol, playerToken);
				break;
			}
			if (b.checkIfWinning(playerToken) == true) {
				System.out.println(b);
				System.out.println("Player "+player+" is the winner");
				gameOver = true;
			} else if (b.checkIfFull() == true) {
				System.out.println("It's a tie!");
				gameOver = true;
			} else {
				player = player + 1;
				if (player > 2) {
					player = 1;
				}
			}
		}
	}
}