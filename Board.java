public class Board {
	
	private Square[][] tictactoeBoard;
	
	public Board() {
		this.tictactoeBoard = new Square[3][3];
		for (int i = 0; i < 3; i++) {
			for (int j = 0; j < 3; j++) {
				this.tictactoeBoard[i][j] = Square.BLANK;
				}
			}
	}
	
	public String toString() {
		String s = "   0  1  2\n";
		for (int i = 0; i < 3; i++) {
			s += i + "  ";
			for (int j = 0; j < 3; j++) {
				s += this.tictactoeBoard[i][j]+" ";
				if (j != 2) {
					s += " ";
				}
			}
			if ( i != 2) {
				s += "\n";
			}
		}
		return s;
	}
	
	public boolean placeToken(int row, int col, Square playerToken) {
		if (row >= 3 && col >= 3 && row < 0 && col < 0 || this.tictactoeBoard[row][col] != Square.BLANK) {
			return false;
		} else {
			this.tictactoeBoard[row][col] = playerToken;
			return true; 
		}
	}
	
	public boolean checkIfFull() {
		for (int i = 0; i < this.tictactoeBoard.length; i++) {
			for (int j = 0; j < this.tictactoeBoard[i].length; j++) {
				if (this.tictactoeBoard[i][j] == Square.BLANK) {
					return false;
				} 
			}
		}
		return true;
	}
	
	private boolean checkIfWinningHorizontal(Square playerToken) {
		for (int i= 0; i < 3; i++) {
			int count = 0;
			for (int j = 0; j < 3; j++) {
				if (this.tictactoeBoard[i][j] == playerToken) {
					count++;
				}
			}
			if (count == 3) {
				return true;
			}
		}
		return false;		
	}
	
	private boolean checkIfWinningVertical(Square playerToken) {
		for (int i = 0; i < 3; i++) {
		boolean isWinning = true;
			for (int j = 0; j < 3; j++) {
				if (tictactoeBoard[j][i] != playerToken) {
					isWinning = false;
					break;
					}
				}
			if (isWinning) {
			return true;
			}	
		}
		return false;
	}
	
	public boolean checkIfWinning(Square playerToken) {
		if (checkIfWinningHorizontal(playerToken) || checkIfWinningVertical(playerToken)) {
			return true;
		} else {
			return false;
		}
	}
	
}